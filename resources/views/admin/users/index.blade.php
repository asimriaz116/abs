@extends('admin.layout')
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Users</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Users</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <!-- /.card -->
                        <div class="card">
                            <div class="card-header">
{{--                                <h3 class="card-title">Users</h3>--}}
                                <div class=" float-right">
                                    <a class="btn btn-success" href="{{ route('users.create') }}"> Create New User</a>
                                </div>
                            </div>

                            <!-- /.card-header -->
                            <div class="card-body">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Roles</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($data as $key => $user)

                                        <tr>

                                            <td>{{ $user->name }}</td>
                                            <td>{{ $user->email }}</td>
                                            <td>
                                                @if(!empty($user->getRoleNames()))
                                                    @foreach($user->getRoleNames() as $v)
                                                        <label class="badge badge-success">{{ $v }}</label>
                                                    @endforeach
                                                @endif
                                            </td>
                                            <td>
{{--                                                <a class="btn btn-info btn-sm"--}}
{{--                                                   href="{{ route('users.show',$user->id) }}">Show</a>--}}
                                                <a class="btn btn-primary btn-sm" href="{{ route('users.edit',$user->id) }}">Edit</a>
                                                {!! Form::open(['method' => 'DELETE','route' => ['users.destroy', $user->id],'style'=>'display:inline']) !!}
                                                {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-sm']) !!}
                                                {!! Form::close() !!}
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>



    {{--    <div class="row">--}}
    {{--        <div class="col-lg-12 margin-tb">--}}
    {{--            <div class="pull-left">--}}
    {{--                <h2>Users Management</h2>--}}
    {{--            </div>--}}
    {{--            <div class="pull-right">--}}
    {{--                <a class="btn btn-success" href="{{ route('users.create') }}"> Create New User</a>--}}
    {{--            </div>--}}
    {{--        </div>--}}
    {{--    </div>--}}


    {{--    @if ($message = Session::get('success'))--}}
    {{--        <div class="alert alert-success">--}}
    {{--            <p>{{ $message }}</p>--}}
    {{--        </div>--}}
    {{--    @endif--}}


    {{--    <table class="table table-bordered">--}}
    {{--        <tr>--}}
    {{--            <th>No</th>--}}
    {{--            <th>Name</th>--}}
    {{--            <th>Email</th>--}}
    {{--            <th>Roles</th>--}}
    {{--            <th width="280px">Action</th>--}}
    {{--        </tr>--}}
    {{--        @foreach ($data as $key => $user)--}}
    {{--            <tr>--}}
    {{--                <td>{{ ++$i }}</td>--}}
    {{--                <td>{{ $user->name }}</td>--}}
    {{--                <td>{{ $user->email }}</td>--}}
    {{--                <td>--}}
    {{--                    @if(!empty($user->getRoleNames()))--}}
    {{--                        @foreach($user->getRoleNames() as $v)--}}
    {{--                            <label class="badge badge-success">{{ $v }}</label>--}}
    {{--                        @endforeach--}}
    {{--                    @endif--}}
    {{--                </td>--}}
    {{--                <td>--}}
    {{--                    <a class="btn btn-info" href="{{ route('users.show',$user->id) }}">Show</a>--}}
    {{--                    <a class="btn btn-primary" href="{{ route('users.edit',$user->id) }}">Edit</a>--}}
    {{--                    {!! Form::open(['method' => 'DELETE','route' => ['users.destroy', $user->id],'style'=>'display:inline']) !!}--}}
    {{--                    {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}--}}
    {{--                    {!! Form::close() !!}--}}
    {{--                </td>--}}
    {{--            </tr>--}}
    {{--        @endforeach--}}
    {{--    </table>--}}


    {{--    {!! $data->render() !!}--}}


@endsection



