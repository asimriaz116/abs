   <!-- Header -->
    @include('admin/common/header')

    <!-- Sidebar -->
    @include('admin/common/sidebar')

    <!-- Content Wrapper. Contains page content -->
      <!-- /.content-wrapper -->
         @yield('content');
    <!-- Footer -->
    @include('admin/common/footer')

