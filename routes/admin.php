<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "admin" middleware group. Now create something great!
|
*/
Route::group(['namespace' => 'admin', 'prefix' => 'admin'], function()
{
    Route::get('/login', 'AuthController@login')->name('admin-login');
    Route::post('/login/submit', 'AuthController@submitLogin')->name('admin-login-submit');
    Route::get('/forgot', 'AuthController@showForgotForm')->name('admin-forgot');
    Route::post('/forgot/submit', 'AuthController@forgot')->name('admin-forgot-submit');
    Route::group(['middleware' => 'auth'], function () {
        // DASHBOARD
        Route::get('/dashboard', 'DashboardController@dashboard')->name('admin-dashboard');
        Route::resource('roles','RoleController');
        Route::resource('users','UserController');
        // PRODUCT
        Route::resource('products','ProductController');

//        Route::get('/product', 'ProductController@index')->name('product-list');
//        Route::get('/product/create', 'ProductController@create')->name('product-create');
//        Route::post('/product/store', 'ProductController@store')->name('product-store');

        Route::get('/logout', 'AuthController@logout')->name('admin-logout');
    });
});


