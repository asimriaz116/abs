<?php

namespace Tests\Browser\Admin;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class LoginTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    /** @test */
    public function a_user_can_log_in()
    {
        $user = factory(App\User::class)->create([
            'email' => 'john@example.com',
            'password' => bcrypt('testpass123')
        ]);

        $this->visit(route('admin-login'))
            ->type($user->email, 'email')
            ->type('testpass1234', 'password')
            ->press('Login')
            ->see('Successfully logged in')
            ->onPage('/dashboard');
    }
}
