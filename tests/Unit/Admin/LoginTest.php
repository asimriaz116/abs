<?php

namespace Tests\Unit\Admin;

use Tests\TestCase;

class LoginTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */

    public function testAdmin()
    {
        $this->get('admin/login');
        $response = $this->post(route('admin-login-submit'), [
            'email' => 'iasimiraz@gmail.com',
            'password' => '123456',
        ]);
        $response->assertRedirect('admin/dashboard');
    }
}
