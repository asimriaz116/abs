<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;
use App\Repositories\Admin\AuthRepository;
use Auth;



class AuthController extends Controller
{
    public function __construct(AuthRepository $auth)
    {
        $this->middleware('guest', ['except' => ['logout']]);
        $this->auth=$auth;
    }
    public function login()
    {
        return view("admin.login");
    }
    public function submitLogin(Request $request)
    {
        $validator = Validator::make(
            array(
                'email' => $request->email,
                'password' => $request->password
            ),
            array(
                'email' => 'required | email',
                'password' => 'required',
            )
        );
        //check validation
        if ($validator->fails()) {
            return redirect('admin/login')->withErrors($validator)->withInput();
        } else {
            //check authentication of email and password
            if ($this->auth->userLogin($request)) {

                return redirect()->intended('admin/dashboard');
            } else {
                return redirect('admin/login')->with('loginError', Lang::get("labels.EmailPasswordIncorrectText"));
            }
        }

    }
    //logout
    public function logout(){
        Auth::guard()->logout();
        return redirect()->intended('admin/login');
    }

}
