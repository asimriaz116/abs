<?php

namespace App\Repositories\Admin;

use Illuminate\Http\Request;
use Auth;

/**
 * Class AuthRepository.
 */
class AuthRepository
{
    public  function  userLogin(Request $request){
        $credentials = $request->only('email', 'password');
        return Auth::guard()->attempt($credentials);
    }
}
