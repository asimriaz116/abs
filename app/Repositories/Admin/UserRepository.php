<?php

namespace App\Repositories\Admin;

use App\Models\User;
use Illuminate\Http\Request;
use DB;
use Auth;
use Spatie\Permission\Models\Role;

/**
 * Class AuthRepository.
 */
class UserRepository
{
    public function all(){
        return User::orderBy('id','DESC')->get();
    }
    public function save($request,$input){
        $user = User::create($input);
        $user->assignRole($request->input('roles'));
    }
    public function show($id){
        return User::find($id);
    }
    public function edit($id){
        $user = User::find($id);
        $roles = Role::pluck('name','name')->all();
        $userRole = $user->roles->pluck('name','name')->all();
        return array($user,$roles,$userRole);
    }
    public function update($request,$input,$id){
        $user = User::find($id);
        $user->update($input);
        DB::table('model_has_roles')->where('model_id',$id)->delete();
        return $user->assignRole($request->input('roles'));
    }
    public function delete($id){
       return User::find($id)->delete();
    }
}
